/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package org.apache.geronimo.kernel.config;


/**
 * The Ymir Geronimo brand.
 * @author Trygve Sanne Hardersen <trygve@hypobytes.com>
 *
 */
public class ReBranding implements Branding.Brand {

	private static final long serialVersionUID = 3L;

	private static final String name = "Ymir";
	
	private static final String lname = "Ymir Application Server";
	
	private static final String tmpdir = "var/tmp";
	
	private static final String jettyhttp = "HTTP Connector";
	
	private static final String jettyhttps = "HTTPS Connector";
	
	private static final String jettyajp = "AJP Connector";
	
	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.Branding.Brand#name()
	 */
	@Override
	public String name() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.Branding.Brand#lname()
	 */
	@Override
	public String lname() {
		return lname;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.Branding.Brand#tmpdir()
	 */
	@Override
	public String tmpdir() {
		return tmpdir;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.GeronimoBrand#jettyAJPConnector()
	 */
	@Override
	public String jettyAJPConnector() {
		return jettyajp;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.GeronimoBrand#jettyHTTPConnector()
	 */
	@Override
	public String jettyHTTPConnector() {
		return jettyhttp;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.GeronimoBrand#jettyHTTPSConnector()
	 */
	@Override
	public String jettyHTTPSConnector() {
		return jettyhttps;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.Branding.Brand#tomcatAJPConnector()
	 */
	@Override
	public String tomcatAJPConnector() {
		return Branding.DEFAULT_TOMCAT_AJP_CONNECTOR;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.Branding.Brand#tomcatHTTPConnector()
	 */
	@Override
	public String tomcatHTTPConnector() {
		return Branding.DEFAULT_TOMCAT_HTTP_CONNECTOR;
	}

	/* (non-Javadoc)
	 * @see org.apache.geronimo.kernel.config.Branding.Brand#tomcatHTTPSConnector()
	 */
	@Override
	public String tomcatHTTPSConnector() {
		return Branding.DEFAULT_TOMCAT_HTTPS_CONNECTOR;
	}
}
