/**
 *	Copyright 2010 HypoBytes Ltd.
 *
 *	Licensed to HypoBytes Ltd. under one or more contributor
 *	license agreements.  See the NOTICE file distributed with
 *	this work for additional information regarding copyright
 *	ownership.
 *
 *	HypoBytes Ltd. licenses this file to You under the
 *	Apache License, Version 2.0 (the "License"); you may not
 *	use this file except in compliance with the License.
 *
 *	You may obtain a copy of the License at:
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *		https://hypobytes.com/licenses/APACHE-2.0
 *
 *	Unless required by applicable law or agreed to in writing,
 *	software distributed under the License is distributed on an
 *	"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *	KIND, either express or implied.  See the License for the
 *	specific language governing permissions and limitations
 *	under the License.
 */
package org.apache.geronimo.kernel.config.test;

import org.apache.geronimo.kernel.config.Branding;
import org.apache.geronimo.kernel.config.ReBranding;
import org.apache.geronimo.kernel.config.Branding.Brand;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test for the HypoSS Geronimo branding.
 * @author Trygve Sanne Hardersen <trygve@hypobytes.com>
 *
 */
public class ReBrandingTest {

	/**
	 * Tests the {@link ReBranding ReBranding} class.
	 */
	@Test
	public void testReBranding(){
		Brand brand = new ReBranding();
		Assert.assertEquals(brand.name(), "Ymir");
		Assert.assertEquals(brand.lname(), "Ymir Application Server");
		Assert.assertEquals(brand.tmpdir(), "var/tmp");
		Assert.assertEquals(brand.jettyHTTPConnector(), "HTTP Connector");
		Assert.assertEquals(brand.jettyHTTPSConnector(), "HTTPS Connector");
		Assert.assertEquals(brand.jettyAJPConnector(), "AJP Connector");
		Assert.assertEquals(brand.tomcatHTTPConnector(), Branding.DEFAULT_TOMCAT_HTTP_CONNECTOR);
		Assert.assertEquals(brand.tomcatHTTPSConnector(), Branding.DEFAULT_TOMCAT_HTTPS_CONNECTOR);
		Assert.assertEquals(brand.tomcatAJPConnector(), Branding.DEFAULT_TOMCAT_AJP_CONNECTOR);
	}
}
